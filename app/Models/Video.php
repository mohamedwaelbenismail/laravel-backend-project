<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    public $timestamps = true;
    protected $table = 'Video';
    protected $primaryKey = 'video_id';
    protected $fillable=['video_name','video_description','video_url','playlist_id','video_length'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
