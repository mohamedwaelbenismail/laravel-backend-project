<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    public $timestamps = true;
    protected $table = 'Categorie';
    protected $primaryKey = 'categorie_id';
    protected $fillable=['categorie_name','categorie_description'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
