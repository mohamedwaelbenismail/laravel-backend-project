<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public $timestamps = true;
    protected $table = 'Course';
    protected $primaryKey = 'course_id';
    protected $fillable=['course_name','course_description','playlist_id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
