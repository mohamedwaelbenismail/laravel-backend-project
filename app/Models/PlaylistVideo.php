<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaylistVideo extends Model
{
    public $timestamps = true;
    protected $table = 'Playlist_Video';
    protected $primaryKey = 'playlist_video_id';
    protected $fillable=['playlist_id','video_id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
