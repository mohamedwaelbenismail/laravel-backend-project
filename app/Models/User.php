<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    public $timestamps = true;
    protected $table = 'User';
    protected $primaryKey = 'user_id';
    protected $fillable=['first_name','last_name','email','photoUrl'];
    protected $hidden=['password'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
