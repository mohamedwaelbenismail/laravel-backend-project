<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    public $timestamps = true;
    protected $table = 'Playlist';
    protected $primaryKey = 'playlist_id';
    protected $fillable=['playlist_name','playlist_description','course_id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
