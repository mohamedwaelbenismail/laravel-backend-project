<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CategorieTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Categorie')->insert([
            'categorie_name'=>'Dev',
            'categorie_description'=>'categorie related to application developpement'
        ]);
    }
}
