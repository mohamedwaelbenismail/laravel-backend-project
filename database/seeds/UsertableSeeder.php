<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsertableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('User')->insert([
            'first_name'=>'Mohamed',
            'last_name'=>'Ben Ismail',
            'email'=>'mohamedwaelbenismail@gmail.com',
            'passowrd'=>bcrypt('Mohamed Ben Ismail')
        ]);
    }
}
