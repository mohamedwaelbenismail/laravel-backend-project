<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Course', function (Blueprint $table) {
            $table->id('course_id');
            $table->string('course_name');
            $table->text('course_description');
            $table->unsignedBigInteger('playlist_id')->nullable()->default(null);
            $table->foreign("playlist_id")->references("playlist_id")->on("Playlist")->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_course');
    }
}
