<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlaylistVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Playlist_Video', function (Blueprint $table) {
            $table->id('playlist_video_id');

            $table->unsignedBigInteger('playlist_id');
            $table->foreign('playlist_id')->references('playlist_id')->on('Playlist')->onDelete('cascade');

            $table->unsignedBigInteger('video_id');
            $table->foreign('video_id')->references('video_id')->on('Video')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('playlist_video');
    }
}
